// Copyright Epic Games, Inc. All Rights Reserved.

#include "AbstractFactoryGameMode.h"
#include "AbstractFactoryHUD.h"
#include "AbstractFactoryCharacter.h"
#include "UObject/ConstructorHelpers.h"

AAbstractFactoryGameMode::AAbstractFactoryGameMode()
	: Super()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnClassFinder(TEXT("/Game/FirstPersonCPP/Blueprints/FirstPersonCharacter"));
	DefaultPawnClass = PlayerPawnClassFinder.Class;

	// use our custom HUD class
	HUDClass = AAbstractFactoryHUD::StaticClass();
}
