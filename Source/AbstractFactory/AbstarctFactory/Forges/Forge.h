// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "AbstractFactory/AbstarctFactory/LootStructures.h"
#include "AbstractFactory/AbstarctFactory/Loot/Elvens/ElvenLoot.h"
#include "AbstractFactory/AbstarctFactory/Loot/Necro/NecroLoot.h"
#include "Engine/DataTable.h"
#include "Components/SphereComponent.h"
#include "Components/TextRenderComponent.h"
#include "Forge.generated.h"

class ALoot;

/**
 * Abstract actor for creating things
 */
UCLASS()
class ABSTRACTFACTORY_API AForge : public AActor
{
	GENERATED_BODY()

protected:
	
	/** default constructor */
	AForge();

	/** OverlapComponent */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Forge")
		USphereComponent* OverlapComponent;

	/** Text Render */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = "Forge")
		UTextRenderComponent* TextRender;
	
public:
	/** Stores data for weapons */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data")
		TMap<UClass*, FArmorInfo> ArmorData;

	/** Stores data for the reservation */
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category = "Data")
		TMap<UClass*, FWeaponInfo> WeaponData;

	/** Creating armor function */
	UFUNCTION(BlueprintPure)
		virtual ALoot* ForgeArmor() PURE_VIRTUAL(AForge::ForgeArmor, return nullptr; );

	/** Creating weapon function */
	UFUNCTION(BlueprintPure)
		virtual ALoot* ForgeWeapon() PURE_VIRTUAL(AForge::ForgeWeapon, return nullptr; );

protected:
	/** Return info for specific class
	 * @return Info Structure for class
	 */
	FBaseInfo* GetInfoForItem(UClass* ItemClass);

	/** Return Armor info
	 *	@return FArmorInfo struct
	 */
	FArmorInfo* GetArmorInfo(UClass* ItemClass);

	/** Return Weapon info
	 *	@return FWeaponInfo struct
	 */
	FWeaponInfo* GetWeaponInfo(UClass* ItemClass);

	/** Called on overlap with OverlapComponent */
	UFUNCTION()
		void OnBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);

};

inline AForge::AForge()
{
	//Create OverlapComponent...
	OverlapComponent = CreateDefaultSubobject<USphereComponent>("OverlapComponent");
	SetRootComponent(OverlapComponent);
	OverlapComponent->OnComponentBeginOverlap.AddDynamic(this, &AForge::OnBegin); //Bind to overlaping event

	//Create Render text...
	TextRender = CreateDefaultSubobject<UTextRenderComponent>(TEXT("TextRender"));
	TextRender->SetupAttachment(OverlapComponent);
	TextRender->SetHorizontalAlignment(EHTA_Center);
}

inline FArmorInfo* AForge::GetArmorInfo(UClass* ItemClass)
{
	if (ItemClass->IsChildOf<AElvenArmor>() || ItemClass->IsChildOf<ANecroArmor>())
	{
		return ArmorData.Find(ItemClass);
	}

	return nullptr;
}

inline FWeaponInfo* AForge::GetWeaponInfo(UClass* ItemClass)
{
	if (ItemClass->IsChildOf<AElvenWeapon>() || ItemClass->IsChildOf<ANecroWeapon>())
	{
		return WeaponData.Find(ItemClass);
	}

	return nullptr;
}

inline FBaseInfo* AForge::GetInfoForItem(UClass* ItemClass)
{
	if(const auto Weapon = GetWeaponInfo(ItemClass))
	{
		return Weapon;
	}
	
	if(const auto Armor = GetArmorInfo(ItemClass))
	{
		return Armor;
	}

	return nullptr;
}

inline void AForge::OnBegin(UPrimitiveComponent* OverlappedComponent, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	ForgeArmor();
	ForgeWeapon();
}

/**
 * Actor creating things for the elves
 */
UCLASS()
class ABSTRACTFACTORY_API AElvenForge : public AForge
{
	GENERATED_BODY()

public:
	virtual ALoot* ForgeArmor() override;
	virtual ALoot* ForgeWeapon() override;
};

inline ALoot* AElvenForge::ForgeArmor()
{
	//Create elven armor item
	auto ItemArmor = GetWorld()->SpawnActor<AElvenArmor>(AElvenArmor::StaticClass());
	if (ItemArmor)
	{
		ItemArmor->SetupWithInfo(GetInfoForItem(AElvenArmor::StaticClass()));
	}
	return ItemArmor;
}

inline ALoot* AElvenForge::ForgeWeapon()
{
	//Create elven armor item
	auto ItemWeapon = GetWorld()->SpawnActor<AElvenWeapon>(AElvenWeapon::StaticClass());
	if (ItemWeapon)
	{
		ItemWeapon->SetupWithInfo(GetInfoForItem(AElvenWeapon::StaticClass()));
	}
	return ItemWeapon;
}

/**
 * Actor creating things for the necro
 */
UCLASS()
class ABSTRACTFACTORY_API ANecroForge : public AForge
{
	GENERATED_BODY()

public:
	virtual ALoot* ForgeArmor() override;
	virtual ALoot* ForgeWeapon() override;
};

inline ALoot* ANecroForge::ForgeArmor()
{
	//Create necro armor item
	auto ItemArmor = GetWorld()->SpawnActor<ANecroArmor>(ANecroArmor::StaticClass());
	if (ItemArmor)
	{
		ItemArmor->SetupWithInfo(GetInfoForItem(ANecroArmor::StaticClass()));
	}
	return ItemArmor;
}

inline ALoot* ANecroForge::ForgeWeapon()
{
	//Create necro weapon item
	auto ItemWeapon = GetWorld()->SpawnActor<ANecroWeapon>(ANecroWeapon::StaticClass());
	if (ItemWeapon)
	{
		ItemWeapon->SetupWithInfo(GetInfoForItem(ANecroWeapon::StaticClass()));
	}
	return ItemWeapon;
}